# Rate My Neighbourhood

Rate My Neighbourhood is built using a [Sails](http://sailsjs.org) back-end, with [Angular](https://angularjs.org) as the front-end for data binding goodness.

Rate My Neighbourhood exists to give users planning to move into an unfamiliar neighbourhood an idea of what the area is like. Rate My Neighbourhood differs from existing services (such as WalkScore), in that Rate My Neighbourhood is based on user-submitted reviews from those who have actually lived there.

Rate My Neighbourhood does not exist as a competitor to existing services. Instead, it strives to complement existing services, helping users to feel as comfortable as possible when purchasing or renting a home in an area unfamiliar to them.