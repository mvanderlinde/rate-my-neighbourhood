module.exports = {

  // Unique name for the model
  identity: 'Rating',
  
  tableName: 'rating',
  
  // The table structure
  attributes: {
    neighbourhood: 'string',
    familyFriendlyRating: 'integer',
    familyFriendlyDetails: 'string',
    amenitiesRating: 'integer',
    amenitiesDetails: 'string',
    safetyRating: 'integer',
    safetyDetails: 'string',
    schoolsRating: 'integer',
    schoolsDetails: 'string',
    noiseRating: 'integer',
    noiseDetails: 'string'
  }
};