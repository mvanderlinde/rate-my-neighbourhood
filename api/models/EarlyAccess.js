module.exports = {

  // Unique name for the model
  identity: 'EarlyAccess',
  
  tableName: 'early_access',
  
  // The table structure
  attributes: {
    email: { 
    type: 'email',
    unique: true,
    required: true
    }
  }
};