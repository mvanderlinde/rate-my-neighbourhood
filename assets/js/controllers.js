'use strict';

/* Controllers */

angular.module('rateMyNeighbourhood.controllers', [])
  .controller('EarlyAccessCtrl', ['$scope', '$http', function($scope, $http) {
  		
  		$scope.data = {
	  		email: "default"
  		}
  		
	  	$scope.submitData = function(data)
	  	{
	  		 	console.log(data);
			  	$http.post("/EarlyAccess", JSON.stringify(data))
			  		.success(function()
			  		{
				  		window.location = "/#/early-access-success";
			  		})
			  		.error(function()
			  		{
				  		alert("Error! Please try again.");
				  		console.log("Error submitting email.");
			  	});
	  	}
  }]);
