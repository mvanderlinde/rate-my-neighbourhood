'use strict';


// Declare app level module which depends on filters, and services
angular.module('rateMyNeighbourhood', [
  'ngRoute',
  'rateMyNeighbourhood.filters',
  'rateMyNeighbourhood.services',
  'rateMyNeighbourhood.directives',
  'rateMyNeighbourhood.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {templateUrl: 'partials/early-access-form.html', controller: 'EarlyAccessCtrl'});
  $routeProvider.when('/early-access-success', {templateUrl: 'partials/early-access-success.html', controller: 'EarlyAccessCtrl'});
  $routeProvider.otherwise({redirectTo: '/'});
}]);
